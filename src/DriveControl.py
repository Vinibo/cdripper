import os
import subprocess

class DriveControl:
    def __init__(self, arguments):
        self.drive = arguments['drive']

    def get_media_info(self):
        return subprocess.run(['cd-info', '--no-device-info', '--no-header',
                                '--no-tracks', '--no-analyze', '--no-ioctl',
                                f'-C {self.drive}'], 
                                stdout=subprocess.PIPE, text=True)

    def eject_media(self):
        os.system(f"eject {self.drive}")