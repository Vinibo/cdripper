#!/usr/bin/env python
import argparse
import re
import logging

from Config import Config
from DriveControl import DriveControl
from Audio.AudioRipper import AudioRipper

disc_type_pattern = re.compile("^Disc mode .*: (.*)$", re.MULTILINE)

parser = argparse.ArgumentParser(description='Orchestrate ripping with abcde and tagging with beets')
parser.add_argument('--drive', help='Path of drive (/dev/sr0)')
parser.add_argument('--musicinput', help='Indicate where abcde output files')
parser.add_argument('--eject', action='store_true', help='Eject media at the end')
parser.add_argument('--abcde_binary', help='Path of abcde binary')
parser.add_argument('--abcde_config', help='Configuration path for abcde')
parser.add_argument('--config', help='Path of configuration for cdripper')
arguments = parser.parse_args()

config = Config(arguments)
drive_control = DriveControl(config.drive_config())

def main():
    media_type = detect_media()
    logging.info(f"Detected media {media_type}")
    if media_type == "CD-DA":
        AudioRipper(config).rip()

    if config.drive_config()['eject_after'] and media_type is not None:
        drive_control.eject_media()


def detect_media():
    media_type = drive_control.get_media_info()
    detected_type = disc_type_pattern.search(media_type.stdout)
    if media_type.returncode == 0 and detected_type:
        return detected_type.group(1)

    return None

if __name__ == "__main__":
    main()
