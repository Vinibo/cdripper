import configparser

class Config:
    def __init__(self, arguments):
        self.config = configparser.ConfigParser()
        self.config.read(arguments.config)

    def drive_config(self):
        return self.config['drive']

    def abcde_config(self):
        return self.config['abcde']

    def beets_config(self):
        return self.config['beets']