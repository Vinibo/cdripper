import os
import subprocess
import hashlib
from pathlib import Path
from mutagen.flac import FLAC

from Audio.AbcdeControl import AbcdeControl
from Audio.BeetsControl import BeetsControl

class AudioRipper:
    def __init__(self, config):
        self.abcde_output_directory = config.abcde_config()['output_directory']
        self.unknown_artist_folder = "/Unknown Artist"
        self.unknown_album_folder = "/Unknown Album"
        self.abcde_control = AbcdeControl(config.abcde_config())
        self.beets_control = BeetsControl(config.beets_config())

    def rip(self):
        print("Handling audio disc")
        ripping_status = self.abcde_control.rip()
        if not self.__tagging_successful():
            self.__handle_untagged_albums()
        
        self.beets_control.tag()

    def __tagging_successful(self):
        failed_tagging_path = Path(self.__get_complete_path())
        return not failed_tagging_path.exists()

    def __handle_untagged_albums(self):
        print("Handling untagged files")
        self.found_tracks = os.listdir(self.__get_complete_path())
        self.__tag_untagged_tracks()

    def __get_complete_path(self):
        return self.abcde_output_directory + self.unknown_artist_folder + self.unknown_album_folder

    def __tag_untagged_tracks(self):
        for t in self.found_tracks:
            full_track_path = self.__get_complete_path() + '/' + t
            track = FLAC(full_track_path)
            track["ALBUM"] = self.__find_best_album_name(track)
            track.save()

    def __find_best_album_name(self, track):
        if track["CDDB"]:
            return track["CDDB"]
        else:
            return self.__compute_md5_from_first_track()

    def __compute_md5_from_first_track(self):
        first_music_track = self.__get_complete_path() + "/" + self.found_tracks[0]
        sha256_hash = hashlib.sha256()
        with open(first_music_track, "rb") as f:
            for byte_block in iter(lambda: f.read(4096), b""):
                sha256_hash.update(byte_block)
        return sha256_hash.hexdigest()
