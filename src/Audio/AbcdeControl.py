import logging
import subprocess

class AbcdeControl:
    def __init__(self, config):
        self.binary = config['binary']
        self.config = config['config']

    def rip(self):
        logging.info(f'Ripping audio with {self.binary} with config {self.config}')
        return subprocess.run([self.binary, '-c', self.config],  stdout=subprocess.DEVNULL)